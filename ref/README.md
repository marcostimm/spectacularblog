Full Stack Developer Assignment
==================

## Introduction
For this assignment, we'd like you to demonstrate your skills in the following:
* HTML
* CSS
* Javascript
* PHP
* OOP
* Git
* Documentation

For this assignment, you should follow your process and standards. 
Please keep in mind that we'd like you to deliver a "minimum lovable product" that can still prove your attitude towards quality.

**Please note**
1. We consider all requirements in this assignment equally important and check that they are all met, since the special requirements clarify to us your technical and logical thinking skills. So make sure to read carefully!
2. Go about your solution whichever way you prefer but please keep in mind that we need to see your coding abilities - configuring Laravel alone won't cut it
3. Make sure to include a readme file with your final product that holds instructions on how to set up your application on a local machine - if we can't see it work we cannot hire you
4. Feel free to add extra documentation about what you built

Good luck!

## Assignment
Build a small blog consisting of only one responsive page. 
The structure of the page and all the information to be displayed are shown in the wireframes.
The wireframes also indicate the supported viewports that need to be implemented. They can be found in the `/wireframes` directory.
Do not use a database to save the information, but find another solution.

On this blog, a form should be available to submit a new post without the browser refreshing the page.
The page should display all the posts starting from the most recent.
**Please note:** If the user refreshes the page, the information already posted should be preserved.

The form consists of the following input fields:
* Title
* Content
* Image upload (supporting only jpg and png)
* Email address

It should not be possible to submit the form if:
* The Email address is missing or incorrect
* The Title is missing
* Both the content and the Image fields are empty

In case of any validation error, the user should receive feedback and the submission should not take place. 

During the form submission, the user should be informed that a process is happening: provide some visual animated feedback.

After submitting, the PHP backend of your blog will perform another round of validation.
Specifically it will check that:
* The Email address submitted equals the one of the blog owner
* The uploaded file is of a supported type: either jpg or png
* The post was submitted by an authorized source, i.e. the blog frontend

After the post has been validated:
* Attach a timestamp to it (YYYY/MM/DD, HH:MM)
* Process the post content by:
  * Process the image so that it can fit in a 300x300px square, saving it and discarding the original file
  * Creating the front end markup that will be returned (see wireframe)
  * Save all the words that are longer than 4 characters so that the five most used words across all posts are displayed on the frontend
* Return all the information to the frontend and:
  * Make sure that the post is displayed as the latest one
  * Update the list of the most used words

### Bonus tasks for extra points
* Try not using PHP or JS frameworks to accomplish this 
* Build the visual animated feedback for the form submission with CSS only
* Use our style library ARC to:
  * Implement responsiveness with our grid component
  * Style the look and feel of the page with one or more of our components
  * Provide visual feedback for the form validation (error/success messages)
  * Documentation about ARC components and how to use them is avalable at: http://albumprinter.github.io/arc/ (**hint:** the documentation site itself is built with ARC)
