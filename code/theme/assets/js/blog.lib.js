/**
 * Just a scope
 */
(function() {

	/**
	 * @file Spectacular Blog
	 * @author Marcos Timm <timm@marcos.im>
	 * @version 0.1
	 */
 	this.Spectacular = function(options, callback) {

		this.Spectacular		= null;

        const server 			= 'http://localhost/index.php'

        /**
		 * Get all form objects (this can be configurated with a 'options' variable in the future version)
		 */
        const savePost 			= document.getElementById("save")
        const title				= document.getElementById("title")
        const post				= document.getElementById("post")
        const email				= document.getElementById("email")
        const file				= document.getElementById("imageFile")
        const imageName			= document.getElementById("imageName")
        const removeImage		= document.getElementById("removeImage")
        const uploadImage		= document.getElementById("uploadImage")
        const uploadProgress	= document.getElementById("uploadProgress")
        const error		        = document.getElementById("error")
        const errorMsg	        = document.getElementById("errorMsg")
        const token				= document.getElementById("_token")
        // get form validation elements
        const titleError 		= document.getElementById("title-error")
        const postError 		= document.getElementById("post-error")
        const emailError 		= document.getElementById("email-error")
        const fileError 		= document.getElementById("file-error")
        const spinner			= document.getElementById("spinner")

        /**
		 * Method for load the first blog data using XMLHttpRequest (this request can be abstracted in the future)
		 */
        this.loadBlog = function() {
        	_this = this;

        	this.initializeEvents();

			var xhr = new XMLHttpRequest();
			xhr.open("GET", server + "?loadblog=true", true);

			xhr.onreadystatechange = function (e) {
				if (this.readyState != 4) return;
				
				if (this.status == 200) {
    				var data = JSON.parse(this.responseText);
    				console.log(data)

    				if(data["posts"]) {
    					for (i = data["posts"].length; i > 0 ; i--) {
    						_this.includePost(data["posts"][i-1])
    					}
    				}
    				if(data["most_used_words"]) {
    					_this.updateUsedWords(data["most_used_words"]);
    				}
				} else {
					console.log("Log Data problem")
					console.log(this.responseText)
				}
			};
  			xhr.send();
        }

        /**
		 * Method for initialize events
		 */
		this.initializeEvents = function() {
			var _this = this

	        /**
			 * Handler with change event on input File field
			 */
	        file.addEventListener('change', function() {
				if (file.value) {
					removeImage.classList.remove("hide");
					uploadImage.disabled = false
				}
	        });
	        /**
			 * Handler with the action for remove file from input file field
			 */
	        removeImage.addEventListener('click', function() {
	        	file.value = "";
	        	removeImage.classList.add("hide")
	        });
	        /**
			 * Handler with Save Post button.
			 */
			savePost.addEventListener('click', function () {

				// Validate all form field
				if(_this.validateForm()){

					savePost.style.display = "none";
					spinner.classList.remove("hide")

					// Send a XMLHttpRequest with post data
					var xhr = new XMLHttpRequest();
					xhr.open("POST", server, true);
					xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

					xhr.onreadystatechange = function (e) {
	    				if (this.readyState != 4) return;
	    				spinner.classList.add("hide")
	    				savePost.style.display = "inline-block";
	                    var data = JSON.parse(this.responseText);
	    				if (this.status == 200) {
							_this.resetFormPost()
							_this.includePost(data.post)
							_this.updateUsedWords(data.most_used_words)

	    				} else {
	                        errorMsg.innerHTML = data.description ? data.description : "Unexpected error";
	                        error.classList.remove("hide");
	                        setTimeout(function(){ error.classList.add("hide"); }, 10000);
	    				}
					};

					var params = "title="+title.value+"&post="+post.value+"&email="+email.value+"&image="+imageName.value+"&token="+token.value;
					xhr.send(params);
	        	}

			});
	        /**
			 * Handler with image upload button
			 */
			uploadImage.addEventListener('click', function() {
				if(file.value) {
					file.classList.remove('error');
	    			fileError.classList.add("hide")
	    			_this.uploadFile()
				} else {
	    			document.getElementById("file-error-message").innerHTML = "required"
	    			file.classList.add('error');
	    			fileError.classList.remove("hide")
				}
			});

		}

        /**
		 * Method for update the progresso of status bar
		 */
		this.updateProgress = function(percent) {
			document.getElementById("progressBar").style.width = percent + "%";
		}

        /**
		 * Method for updload image with a object FormData
		 */
		this.uploadFile = function() {
			var _this = this
  			var xhr 		= new XMLHttpRequest()
  			var formData 	= new FormData()
  			xhr.open('POST', server, true)

			document.getElementById("progressBar").style.width = "0px";
  			xhr.upload.addEventListener("progress", function(e) {
    			_this.updateProgress((e.loaded * 100.0 / e.total) || 100)
  			});

  			xhr.addEventListener('readystatechange', function(e) {
  				if (xhr.readyState != 4) return;
    			imageName.value = ""
	      		var resp = JSON.parse(xhr.responseText)
    			if (xhr.status == 200) {
	      			if(resp.success) {
		      			imageName.value = resp.image
						savePost.disabled = false	      				
					}
    			} else {
      				uploadImage.disabled = false
      				removeImage.classList.remove("hide")
    			}
				errorMsg.innerHTML = resp.description
				error.classList.remove("hide");
				setTimeout(function(){ error.classList.add("hide"); }, 2000);
    			uploadProgress.classList.add("hide");
  			});

  			formData.append('file', file.files[0])
  			xhr.send(formData)

  			uploadProgress.classList.remove("hide")
  			uploadImage.disabled = "disabled"
  			removeImage.classList.add("hide")
  			savePost.disabled = "disabled"
		}

        /**
		 * Method for update the list of most used words
		 */
		this.updateUsedWords = function(words) {
			var list = document.getElementById("mostUsedList")
			list.innerHTML = ""
			for (var i=0; i < words.length; i++) {
				var listEl = document.createElement("li")
				listEl.innerHTML = words[i]
				list.appendChild(listEl)
			}
		}

        /**
		 * Method for insert a new post using a html template at the beginner of post list 
		 */
		this.includePost = function(post) {
			var postWrapper 			= document.getElementById("postWrapper")
			var newPostTemplate 		= document.createElement('div');
    		newPostTemplate.innerHTML 	= '<article itemscope itemtype="http://schema.org/BlogPosting" class="panel post"><div class="row"><div class="large-10 small-9 columns"><h3 class="post-title" itemprop="name">'+post.title+'</h3></div><div class="large-2 small-3 columns"><p class="tertiary" itemprop="datePublished">'+post.created_formated+'</p></div></div><div class="row"><div class="columns"><img src="'+post.image+'" alt="Image"><p itemprop="articleBody">'+post.content+'</p></div></div></article>';
    		postWrapper.insertBefore(newPostTemplate, postWrapper.firstChild)			
		}

        /**
		 * Method for clean all form field
		 */
		this.resetFormPost = function() {
			title.value			= ""
        	post.value 			= ""
        	email.value 		= ""
        	file.value 			= ""
        	imageName.value 	= ""

        	uploadImage.disabled = "disabled"
		}

        /**
		 * Method for validate all form fields and display a error message if needed
		 */
		this.validateForm = function() {
        	var error = 0
        	title.classList.remove("error")
        	titleError.classList.add("hide")
        	post.classList.remove("error")
        	postError.classList.add("hide")
        	email.classList.remove("error")
        	emailError.classList.add("hide")
        	file.classList.remove("error")
        	fileError.classList.add("hide")

        	if (title.value == "") {
        		title.classList.add('error');
        		titleError.classList.remove("hide")
        		error++
        	}
        	if (post.value == "") {
        		post.classList.add('error');
        		postError.classList.remove("hide")
        		error++
        	}
        	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		if  (!re.test(String(email.value).toLowerCase())) {
    			document.getElementsByClassName("email-error-message")[0].innerHTML = "invalid email"
    			emailError.classList.remove("hide")
    			email.classList.add('error');
    			error++
    		}
        	if (email.value == "") {
        		document.getElementsByClassName("email-error-message")[0].innerHTML = "required"
        		email.classList.add('error');
        		emailError.classList.remove("hide")
        		error++
        	}
    		if (imageName.value == "") {
    			document.getElementsByClassName("file-error-message")[0].innerHTML = "upload image"
    			file.classList.add('error');
    			fileError.classList.remove("hide")
    			error++
    		}
    		if(!file.value) {
    			document.getElementsByClassName("file-error-message")[0].innerHTML = "required"
    			file.classList.add('error');
    			fileError.classList.remove("hide")
    			error++
    		}

        	return error ? false : true
        }

		init(this);

	};

    /**
	 * Method for auto initialize object
	 */
	function init(Spectacular) {
		Spectacular.loadBlog()
    }

}());