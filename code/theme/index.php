<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="ie"> <![endif]-->
<!--[if IE 10]>        <html class="ie"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>My spectacular blog</title>
    <link rel="stylesheet" href="//cdn-files.cloud/arc/css/arc.discovery.min.css" />
    <link rel="stylesheet" href="/theme/assets/css/spectacular.css" />
    <!--[if IE 8]><link rel="stylesheet" href="//cdn-files.cloud/arc/css/arc-ie8fix.min.css" /><![endif]-->
    <script type='text/javascript' src="//cdn-files.cloud/arc/js/jquery.arc.min.js"></script>
    <script type='text/javascript' src="//cdn-files.cloud/arc/js/picturefill.arc.min.js"></script>

    <!-- <script type='text/javascript' src="/theme/assets/js/blog.js"></script> -->
    <script type='text/javascript' src="/theme/assets/js/blog.lib.js"></script>

</head>

<body class="antialiased">

    <div class="row">

        <header>
            <div class="large-12 columns text-center">
                <h1 class="text-center">My spectacular blog</h1>
                <h4 class="subheader">A totally false statement</h4>
            </div>

            <div class="large-12 columns" id="newPost">
                <div class="panel">
                    <h5>New blog post</h5>
                    <div class="row">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="large-12 columns">
                                <div class="row">
                                    <div class="large-9 small-12 columns">
                                        
                                        <!-- CSRF Protection -->
                                        <input type="hidden" name="_token" id="_token" value="<?php echo $token; ?>">

                                        <div class="row">
                                            <div class="large-10 small-12 columns">
                                                <input type="text" placeholder="My post title" class="" id="title">
                                            </div>
                                            <div class="large-2 columns hide" id="title-error">
                                                <label class="inline show-for-medium-up">
                                                    <small class="error"><i class="small icon form-error"></i> required</small>
                                                </label>
                                                <label class="left show-for-small">
                                                    <small class="error"><i class="small icon form-error"></i> required</small>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="large-10 small-12 columns">
                                                <textarea id="post" class="" placeholder="Here all my important post text!"></textarea>
                                            </div>
                                            <div class="large-2 columns hide" id="post-error">
                                                <label class="inline show-for-medium-up">
                                                    <small class="error"><i class="small icon form-error"></i> required</small>
                                                </label>
                                                <label class="left show-for-small">
                                                    <small class="error"><i class="small icon form-error"></i> required</small>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="large-10 small-12 columns">
                                                <input type="email" placeholder="Email Address" class="" id="email">
                                            </div>
                                            <div class="large-2 columns hide" id="email-error">
                                                <label class="inline show-for-medium-up">
                                                    <small class="error"><i class="small icon form-error"></i> <span class="email-error-message">required</span></small>
                                                </label>
                                                <label class="left show-for-small">
                                                    <small class="error"><i class="small icon form-error"></i> <span class="email-error-message">required</span></small>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="large-7 small-9 columns">
                                                <input type="file" name="file" class="" id="imageFile" accept="image/x-png,image/jpeg">
                                                <input type="hidden" id="imageName" value="">
                                                <a id="removeImage" class="small hide">(X remove image)</a>
                                            </div>
                                            <div class="large-3 small-3 columns text-right">
                                                <button type="button" id="uploadImage" disabled="disabled" class="button tiny">Upload image</button>
                                            </div>
                                            <div class="large-2 columns hide" id="file-error">
                                                <label class="inline show-for-medium-up">
                                                    <small class="error"><i class="small icon form-error"></i> <span class="file-error-message">required</span></small>
                                                </label>
                                                <label class="left show-for-small">
                                                    <small class="error"><i class="small icon form-error"></i> <span class="file-error-message">required</span></small>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row hide" id="error">
                                            <div class="large-10 small-12 columns">
                                                <div data-alert="" class="alert-box warning" id="errorMsg"></div>
                                            </div>
                                        </div>

                                        <div class="row hide" id="uploadProgress">
                                            <div class="large-12 small-12 columns">
                                                <div class="progress large-10 success round">
                                                    <span class="meter" id="progressBar" style="width:0%"></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="large-3 small-12 columns text-center">
                                        <button type="button" class="button hide" id="save">Save post</button>
                                        <div class="spinner hide" id="spinner"></div>
                                    </div>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </header>

        <section class="large-12 columns">

                <div class="row">

                    <div class="large-9 small-12 columns" id="postWrapper"></div>

                    <aside class="large-3 small-12 columns" itemscope itemtype="http://schema.org/SiteNavigationElement">
                        <div class="panel">
                            <h5>Most used words</h5>
                            <ul id="mostUsedList">
                            </ul>
                        </div>
                    </aside>

                </div>
        </section>

    </div>
    <script>
        var Spectacular = new Spectacular();
    </script>
</body>
</html>