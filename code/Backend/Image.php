<?php

namespace Blog;

/**
 *
 * Blog
 *
 * Object representation of a Image
 *
 * @package Blog
 * @author  Marcos Timm <timm@marcos.im>
 *
 */
class Image {

    /**
     * Image path for sabe images
     *
     * @const string
     */
	const imagePath 			= "../../data/images/";

    /**
     * Defaut image square size
     *
     * @const int
     */
	const imageSquareSize 		= 300;

    /**
     * Array with allowed images type
     *
     * @var array
     * @access protected
     */
	protected $allowedImageType = ["image/jpeg","image/png"];

    /**
     * Object image from http form
     *
     * @var object
     * @access private
     */
	private $image 				= null;

	/**
	 * Constructor method
	 *
	 * This method will instantiate the image object
	 *
	 * @var $image Image Object from $_FILES["file"]
	 *
	 * @return void
	 * @access public
	 */
	public function __construct($image) {
		$this->image = $image;
	}

	/**
	 * Validation of image type
	 *
	 * This method will validate if the image is allowed
	 *
	 * @var $image Image Object from $_FILES["file"]
	 *
	 * @return bool
	 * @access private
	 */
	private function isValidImage($file) 
	{
		return in_array($file["type"], $this->allowedImageType);
	}

	/**
	 * Ramdom image name
	 *
	 * This method will generate a random string name for save image in the server
	 *
	 * @return string
	 * @access private
	 */
	private function randomImageName() 
	{
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$charactersLength = strlen($characters);
    	$randomString = '';

    	for ($i = 0; $i < 15; $i++) {
        	$randomString .= $characters[rand(0, $charactersLength - 1)];
    	}

    	return $randomString;
	}

	/**
	 * Upload image
	 *
	 * This method is responsible for upload a new image to server
	 *
	 * @return jSon
	 * @access public
	 */
	public function uploadImage()
	{

		/**
		 * Validate if the image is allowed
		 */
		if ( $this->isValidImage($this->image) ) {
			$filename = $this->randomImageName() . ".png";
			$filePath = dirname(__FILE__) . self::imagePath . $filename;

			// Get info about the image for calculate the center of a new square image
			$info   = getimagesize($this->image['tmp_name']);
			$x = $info[0];
			$y = $info[1];
			$mimeType = $info["mime"];

			// Portrait image
			if ($x > $y) {
    			$square = $y;
    			$offsetX = ($x - $y) / 2;
    			$offsetY = 0;
			}
			// Landscape image
			elseif ($y > $x) {
    			$square = $x;
    			$offsetX = 0;
    			$offsetY = ($y - $x) / 2;
			}
			// Nice, it's a square :)
			else {
    			$square = $x;
    			$offsetX = $offsetY = 0;
			}
			$finalSize = self::imageSquareSize;
			// Create a new empty image 
			$newImage = imagecreatetruecolor($finalSize, $finalSize);
			if ($mimeType == "image/jpeg") {
				$oldImage = imagecreatefromjpeg($this->image['tmp_name']);
			} elseif ($mimeType == "image/png") {
				$oldImage = imagecreatefrompng($this->image['tmp_name']);
			} else {
				header("HTTP/1.0 406 Not Acceptable");
				echo json_encode(['success' => false, 'description' => 'Mimetype not accepted']);				
			}

			// Save the new square image to the server
			if (!imagecopyresampled($newImage, $oldImage, 0, 0, $offsetX, $offsetY, $finalSize, $finalSize, $square, $square)) { 
				header("HTTP/1.0 406 Not Acceptable");
				echo json_encode(['success' => false, 'description' => 'Could not save file']);
			} else {
				if (imagepng($newImage, $filePath, 0)) {
					echo json_encode(['success' => true, 'description' => 'Image saved', 'image' => $filename]);
				} else {
					header("HTTP/1.0 406 Not Acceptable");
					echo json_encode(['success' => false, 'description' => 'Image file could not be saved']);
				}
			}
			exit();

		} else {
			header("HTTP/1.0 406 Not Acceptable");
			echo json_encode(['success' => false, 'description' => 'Image type not allowed']);
			exit();
		}
	}

}

?>