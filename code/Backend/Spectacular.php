<?php

namespace Blog;

use Blog\Post;
use Blog\Image;

/**
 *
 * Spectacular
 *
 * Responsible for manipulating all blog features
 *
 * @package Blog
 * @author  Marcos Timm <timm@marcos.im>
 *
 */
class Spectacular {

	/**
	 * Path of json storage for all content
	 */
	const jsonFile 				= "../../data/blog.json";

	/**
	 * Most used words in all blog post
	 */
	private $most_used_words 	= [];

	/**
	 * Array with posts
	 */
	private $posts 				= [];

	/**
	 * Data of the owner of the blog
	 */
	private $owner 				= [];

	/**
	 * Constructor of Spectacular blog
	 *
	 * Call a method responsable for load all blog data
	 *
	 * @return void
	 */
	public function __construct() 
	{
		$this->getPosts();
	}

	/**
	  *
	  * Generate a random string and save it in a session for check later
	  * CSRF Token prevent Cross-site request forgery
	  *
	  * @return string
	  * @access public
	  */
	public function generateRandomToken() {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 20; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    // Set token to session
	    $_SESSION['_token'] = $randomString;
	    return $randomString;
	}

	/**
	  *
	  * Load all data from jSon storage for construct the blog object
	  *
	  * @return void
	  * @throws exception jSon
	  * @access public
	  */
	public function getPosts() 
	{
		try {
    		$content = file_get_contents(dirname(__FILE__) . self::jsonFile);
    		if ($content === false) {
        		// Content was empty
    		} else {
    			$blog = json_decode($content, true);

    			// Owner blog information
    			$this->owner = $blog["owner"];

				// Post blog information
    			if ($blog["posts"]) {
    				$this->posts = $blog["posts"];
    			}
    			// Most used words
    			if ($blog["most_used_words"]) {
    				$this->most_used_words = $blog["most_used_words"];
    			}
    		}
		} catch (Exception $e) {
    		header("HTTP/1.0 500 Server Error");
			echo json_encode(['success' => false, 'description' => 'Internal error']);
			exit();
		}
	}

	/**
	  * This method load all initial data from object blog
	  *
	  * Load all data from json storage for construct the blog object
	  *
	  * @return jSon
	  * @access public
	  */
	public function getData() 
	{
		echo json_encode(['success' => true, 'posts' => $this->posts, 'most_used_words' => array_keys($this->most_used_words)]);
		exit();
	}

	/**
	  * Validate, set a new post and save to storage
	  *
	  * Load all data from json storage for construct the blog object
	  *
      * @param string $title the string with post title
      * @param string $postContent an string with the post text
      * @param string $email the email from post author
      * @param string $image path of image file
      * @param string $token CSRF Token generate by session
	  *
	  * @return jSon
	  * @access public
	  */
	public function setPost($title, $postContent, $email, $image, $token) 
	{
		// Validate CSRF token
		if ($_SESSION['_token'] != $token) {
    		header("HTTP/1.0 403 Forbidden");
			echo json_encode(['success' => false, 'description' => 'Shame on you. Do not do it.']);
			exit();
		}

		// Validate if email is the owner of the blog
		if ($email != $this->owner["email"]) {
    		header("HTTP/1.0 403 Forbidden");
			echo json_encode(['success' => false, 'description' => 'Sorry, this is not your blog. (for test use: "timm@marcos.im")']);
			exit();
		}

		if (!empty($title) && !empty($postContent) && !empty($email) && !empty($image)) {

			$post = new Post($title, $postContent, $email, $image);
			array_unshift($this->posts, $post->getPost());
			$this->updateUsedWords();

			$this->updateBlogStorage();		

			echo json_encode(['success' => true, 'description' => 'Post saved', 'post' => $post->getPost(), 'most_used_words' => array_keys($this->most_used_words)]);
			exit();
		} else {
			header("HTTP/1.0 417 Expectation Failed");
			echo json_encode(['success' => false, 'description' => 'Required fields are empty']);
			exit();
		}
	}

	/**
	  * Upload a image
	  *
	  * This method instantiate a Image object for upload a new image from frontend
	  *
	  * @return jSon
	  * @access public
	  */
	public function upload() {
		if ($_FILES["file"]) {
			$upload = new Image($_FILES['file']);
			$upload->uploadImage();

		} else {
			header("HTTP/1.0 417 Expectation Failed");
			echo json_encode(['success' => false, 'description' => 'Image not send']);
			exit();
		}

	}

	/**
	  * Update all data from Spectacular object
	  *
	  * This method will save in a jSon file all data of the blog object
	  *
	  * @return void
	  * @access private
	  */
	private function updateBlogStorage() 
	{
		// Encode all data (in a beautiful way with JSON_PRETTY_PRINT)
		$blogObj = json_encode(["owner" => $this->owner, "most_used_words" => $this->most_used_words, "posts" => $this->posts], JSON_PRETTY_PRINT);
		// Simple save in a jSon file (yep, it's can be very heavy and turn the server down, but it's just a MVP test without database)
		file_put_contents(dirname(__FILE__) . self::jsonFile, $blogObj);
	}

	/**
	  * Update local variable with the new count words
	  *
	  * @return void
	  * @access private
	  */
	private function updateUsedWords() {
		$this->most_used_words = $this->countWords();
	}

	/**
	  * Count how many words have in all posts text
	  *
	  * @return void
	  * @access private
	  */
	private function countWords()
	{
		$bigPostText = "";
		foreach ($this->posts AS $post) {
			$bigPostText .= " " . $post["content"];
		}

		// List all words in a array
		$allWords 				= str_word_count($bigPostText,1);
		// Eliminate words with less than four char
		$withMoreThanFourChar 	= array_filter($allWords, array($this, "hasMoreThanFourChar"));

		$wordCount = [];
		// Count how many times each word can be found in our big post
		foreach ($withMoreThanFourChar as $word) {
			$wordCount[$word] = substr_count($bigPostText, $word);
		}
		// Sort array by value desc
		arsort($wordCount);

		// Cut just the first 5 words and return
		return array_slice($wordCount,0,5);
	}

	/**
	  * Method for help eliminate words with less than four characters
	  *
	  * @param string $word a word to be validated
	  *
	  * @return bool
	  * @access private
	  */
	private function hasMoreThanFourChar($word)
	{
    	return (strlen($word) > 4);
	}

}

?>