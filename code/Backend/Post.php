<?php

namespace Blog;

/**
 *
 * Blog
 *
 * Object representation of a Post model
 *
 * @package Blog
 * @author  Marcos Timm <timm@marcos.im>
 *
 */
class Post {

	/**
	 * Path to the images
	 * @const string
	 */
	const path 		= "/data/images/";

    /**
     * Image file name
     *
     * @var string
     */
	private $image 		= "";

    /**
     * Title of post
     *
     * @var string
     */
	private $title 		= "";

    /**
     * Text with post content
     *
     * @var string
     */
	private $content 	= "";

    /**
     * Email of post author
     *
     * @var string
     */
	private $email 		= "";

    /**
     * Datetime of post creation
     *
     * @var string
     */
	private $created 	= "";

	/**
	 * Constructor of Post model
	 *
	 * Set all post variables
	 *
	 * @return void
	 */
	function __construct(
		string $title, 
		string $content, 
		string $email,
		string $image,
		string $created = null
	){
		$this->title 	= $title;
		$this->content 	= $content;
		$this->email 	= $email;
		$this->image 	= $image;
		$this->created = $created ?? date("Y/m/d H:i:s");
	}

	/**
	 * Get method
	 *
	 * Return a array with post data
	 *
	 * @return array
	 * @access public
	 */
	public function getPost() {
		return [
			"title" 	=> $this->title, 
			"content" 	=> $this->content, 
			"email" 	=> $this->email, 
			"image" 	=> self::path . $this->image,
			"created"	=> $this->created,
			"created_formated" => $this->formatDate($this->created)
		];
	}

	/**
	 * Get method
	 *
	 * Return a formated data to be printed on blog screen
	 *
	 * @return array
	 * @access private
	 */
	private function formatDate($date) {
		return date("d/m/Y", strtotime($date));
	}

	/**
	 * Smart methor __toString
	 *
	 * Returns a jSon object for tests
	 *
	 * @return jSon
	 * @access public
	 */
	public function __toString() {
		return json_encode(["title" => $this->title, "content" => $this->content, "email" => $this->email, "image" => $this->image]);
	}

}

?>