<?php

namespace Blog;
use Blog\Spectacular;

session_start();

require __DIR__ . '/vendor/autoload.php';


// Instantiate Spectacular Blog object
$blog = new Spectacular();

// Handler with image upload
if ($_FILES["file"]) {
	$blog->upload();
}

// Handler with save a new post
if ($_POST["title"] && $_POST["token"]) {
	$blog->setPost($_POST["title"], $_POST["post"], $_POST["email"], $_POST["image"], $_POST["token"]);
}

// Handler with load initial data
if ($_GET["loadblog"] == true) {
	$blog->getData();
}

// CSRF Token to prevent Cross-site request forgery
$token = $blog->generateRandomToken();


// Show the Spectacular theme blog
include "theme/index.php";

?>