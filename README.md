# Spectacular Blog

Technical assignment for Albelli recruitment process

### Usage

#### Enter in the `Spectacular Blog` root and Run `docker-compose`

    docker-compose up

### Open

	http://localhost

## Author

* **Marcos Timm Rossow** - *Full Stack Web Developer* - [/marcostimm](https://github.com/marcostimm)